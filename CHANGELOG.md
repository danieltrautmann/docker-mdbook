# Changelog

## docker-mdbook 0.2.3

- [CHANGE] Update mdBook to version 0.3.7
- [CHANGE] Update mdbook-linkcheck to version 0.5.1

## docker-mdbook 0.2.2

- [CHANGE] Update mdBook to version 0.3.6 #3
- [ENHANCEMENT] Provide latest tag for the most recent image #2

## docker-mdbook 0.2.1

- [BUGFIX] Add ca-certificates for mdbook-linkcheck

## docker-mdbook 0.2.0

- [FEATURE] Add mdbook-linkcheck version 0.5.0 for link checking #1
- [CHANGE] Switch versioning mechanism

## docker-mdbook 0.1.0

- [FEATURE] Add mdBook version 0.3.5

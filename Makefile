VERSION := 0.2.3

# mdBook
MDB_VERSION := 0.3.7
MDB_ARCHIVE := mdbook-v$(MDB_VERSION)-x86_64-unknown-linux-gnu.tar.gz
MDB_URL := https://github.com/rust-lang/mdBook/releases/download/v$(MDB_VERSION)/$(MDB_ARCHIVE)

# mdbook-linkcheck
MDBLC_VERSION := 0.5.1
MDBLC_ARCHIVE := mdbook-linkcheck-v$(MDBLC_VERSION)-x86_64-unknown-linux-gnu.tar.gz
MDBLC_URL := https://github.com/Michael-F-Bryan/mdbook-linkcheck/releases/download/v$(MDBLC_VERSION)/$(MDBLC_ARCHIVE)

REPO := registry.gitlab.com/danieltrautmann/docker-mdbook
TAG := $(VERSION)

.DEFAULT_GOAL := image

.PHONY: download
download:
	# mdBook
	curl -sLO $(MDB_URL)
	tar -zxf $(MDB_ARCHIVE)
	# mdbook-linkcheck
	curl -sLO $(MDBLC_URL)
	tar -zxf $(MDBLC_ARCHIVE)

.PHONY: build
build:
	docker build -t $(REPO):$(TAG) .

.PHONY: clean
clean:
	# mdBook
	rm mdbook
	rm $(MDB_ARCHIVE)
	# mdbook-linkcheck
	rm mdbook-linkcheck
	rm $(MDBLC_ARCHIVE)

.PHONY: tag-latest
tag-latest:
	docker tag $(REPO):$(TAG) $(REPO):latest

.PHONY: image
image: download build clean

.PHONY: release-image
release-image: image tag-latest

.PHONY: push
push:
	docker push $(REPO)

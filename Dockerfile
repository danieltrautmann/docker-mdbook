FROM ubuntu:18.04

RUN apt update \
	&& apt install -y --no-install-recommends ca-certificates \
	&& apt clean -y \
	&& rm -rf /tmp/* /var/tmp/*

COPY mdbook /usr/local/bin/mdbook
COPY mdbook-linkcheck /usr/local/bin/mdbook-linkcheck

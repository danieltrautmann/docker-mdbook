# docker-mdbook

This repository holds source files for creating a Docker image that contains the
[mdBook][mdbook] binary and preprocessors.

The main purpose of this Docker image is to generate the files for a website,
where I collect notes about [how I do things][how-i].

## Contents

- [mdBook][mdbook]
- [mdbook-linkcheck][mdbook-linkcheck]

## Usage

The [Makefile](Makefile) contains everything you need.

**Build Image**

```bash
$ make image
```

or

```bash
$ make
```

**Push Image**

Don't forget to login (`docker login registry.gitlab.com`) before.

```bash
$ make push
```

**Release**

Releasing an image includes tagging the most recent version with the `latest`
tag.

```bash
$ make release-image
$ make push
```

[mdbook]: https://github.com/rust-lang/mdBook
[mdbook-linkcheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck
[how-i]: https://gitlab.com/danieltrautmann/how-i
